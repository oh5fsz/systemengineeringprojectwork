from flask import Flask, jsonify, request
from threading import Thread
import os
import paho.mqtt.client as mqtt
import json

app = Flask(__name__)
c = mqtt.Client()

def on_connect(client, userdata, flags, rc):
    print("MQTT connected")

def on_disconnect(client, userdata, rc):
    print('MQTT disconnected')

@app.route('/control/set/', methods=["POST"])
def set_control():
    mode = request.args.get('mode')
    ref = request.args.get('ref')

    if not mode or not ref:
        return jsonify({'detail': 'specify both mode and ref'}), 400
    try:
        ref = float(ref)
        mode = int(mode)
    except:
        return jsonify({'detail': 'invalid request data'}), 400
    if not mode == 0 and not mode == 1:
        return jsonify({'detail': 'invalid control mode'}), 400
    c.publish("control", payload=json.dumps({'control': {'mode': mode, 'ref': ref}}))
    return jsonify({'detail': 'ok'})

if __name__ == "__main__":
    c.on_connect = on_connect
    c.on_disconnect = on_disconnect
    c.connect(os.getenv('MQTT_SERVER', 'mqtt.naukkarinen.xyz'), 1883, 60)
    c.loop_start()
    app.run(host='0.0.0.0', port=8000)
