# SystemEngineeringProjectWork

Backend/frontend for gathering and visualizing measurement data. Also includes motor control via MQTT.

# How to run
1. Make sure you have Docker and docker-compose installed
2. Change volume paths for Grafana/InfluxDB to your liking in docker-compose.yml. Remember to chown the grafana volume path for 472:472
3. Run docker-compose up -d
4. Send production measurements to "measurements/1". If using the included simulator in TestScripts, the data will be sent to measurements/simulated and written to InfluxDB with the motor tag "simulated"
5. Control commands from Grafana will be sent to topic "control"

# Folder structure
Project contains multiple parts, situated in different folders. Root folder contains the docker-compose file to get everything running.

##### TestScripts
- Contains a Python script for sending dummy measurement data.

##### emq_docker
- Contains the Docker build files for EMQX Docker image.

##### mqtt_listener
- Contains the MQTT worker, which receives measurement data and writes them into InfluxDB. Implemented using Python, workers can be scaled with docker-compose --scale

##### minibackend
- A single endpoint backend, which transforms the control command sent from Grafana to a MQTT message.

Database used for measurement data is InfluxDB. InfluxDB is included in the provided docker-compose.yml. Compose file also includes container definition for Grafana, which is used to visualize measurement data and to control the motor.

# Data format
Data is received and sent as JSON.


#### Measurement packets
Measurement data should follow the following format:

```json
[{
	"ac": {
		"I_A": 21.0,
		"I_B": 22.0,
		"I_C": 23.0
	},
	"modulator": {
		"Va": 12.6,
		"Vb": 10.8
	},
	"encoder": {
		"theta": 50.4,
		"omega": 4.6,
		"theta_el": 329.2
	},
	"control": {
		"mode": 0,
		"ref": 1500.0
	},
	"timestamp": "2018-11-11T17:46:18"
}]
```

Data must contain an array of measurements.

Measurement data must have a timestamp and at least one measurement (ac, modulator or encoder).
All data must be float32 (except control mode).

Timestamp should be in ISO 8601 format.

#### Control packets
Control data is sent to the device in the following format:
```json
{
    "control": {
        "mode": 0,
        "ref": 1500.0
    }
}
```

"type" (unsigned integer) defines the type of control, this can be.

- 0 for speed control
- 1 for torque control

"ref" (float32) defines the control reference value. Units are:

- RPM for speed control
- Nm for torque control
