from datetime import datetime
import json
import logging
import os
import random
import time
import paho.mqtt.client as mqtt
from threading import Lock, Event


class Motor(object):
    CONTROL_METHODS = [0, 1]

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.host = os.getenv('MQTT_HOST', 'mqtt.naukkarinen.xyz')
        self.port = int(os.getenv('MQTT_PORT', 1883))
        self._client = mqtt.Client()
        self._client.on_connect = self.mqtt_connect_callback
        self._client.on_message = self.mqtt_message_callback
        self._control_method = 0
        self._ref = 0.0
        self._last_measurement = None
        self._mutex = Lock()
        self.send_period = float(os.getenv('SEND_PERIOD', 5))
        self.dont_send = Event()

    def start(self):
        self._client.connect(self.host, self.port, 60)
        self._client.loop_start()
        while True:
            if not self.dont_send.is_set():
                data = {"measurements": [self.generate_measurement()]}
                self._client.publish(
                    'measurements/simulator', json.dumps(data))
                self._last_measurement = data
                self.log.debug("Sent: %s", self._last_measurement)
            time.sleep(self.send_period)

    @property
    def mock_phase_current(self):
        return 20 + random.uniform(-10, 10)

    @property
    def mock_modulator_voltage(self):
        return 12.0 + random.uniform(-2, 2)

    @property
    def mock_encoder_theta(self):
        return 180.0 + random.uniform(-180, 180)

    @property
    def mock_encoder_omega(self):
        return 5.0 + random.uniform(-2, 2)

    @property
    def control_method(self):
        self._mutex.acquire()
        val = self._control_method
        self._mutex.release()
        return val

    @control_method.setter
    def control_method(self, method):
        if method not in self.CONTROL_METHODS:
            return
        self._mutex.acquire()
        self._control_method = method
        self._mutex.release()
        self.log.debug('Control method set to %f' % self._control_method)

    @property
    def reference(self):
        self._mutex.acquire()
        val = self._ref
        self._mutex.release()
        return val

    @reference.setter
    def reference(self, ref):
        self._mutex.acquire()
        self._ref = ref
        self._mutex.release()
        self.log.debug('Reference set to %f' % self._ref)

    def stop_sending(self):
        self.dont_send.set()
        self.log.debug('Stop sending of data')

    def start_sending(self):
        self.dont_send.clear()
        self.log.debug('Start sending of data')

    def generate_measurement(self):
        return {
            "ac": {
                "I_A": self.mock_phase_current,
                "I_B": self.mock_phase_current,
                "I_C": self.mock_phase_current
            },
            "modulator": {
                "Va": self.mock_modulator_voltage,
                "Vb": self.mock_modulator_voltage
            },
            "encoder": {
                "theta": self.mock_encoder_theta,
                "omega": self.mock_encoder_omega,
                "theta_el": self.mock_encoder_theta,
            },
            "control": {
                "mode": self.control_method,
                "ref": self.reference,
            },
            "timestamp": datetime.utcnow().isoformat()
        }

    def mqtt_connect_callback(self, client, userdata, flags, rc):
        if rc == 0:
            self.log.info('MQTT connected')
        client.subscribe("control")

    def mqtt_message_callback(self, client, userdata, message):
        try:
            self.log.debug('Received: %s on %s' %
                           (message.payload, message.topic))
            data = json.loads(message.payload.decode('utf-8'))
            payload = data.get('control', {})
            if not payload:
                self.log.error('Message contained no payload')
                return

            mode = payload.get('mode', None)
            if mode is not None and int(mode) in self.CONTROL_METHODS:
                self.control_method = int(mode)

            ref = payload.get('ref', None)
            if ref:
                self.reference = int(ref)

            cmd = payload.get('cmd', None)
            if cmd and cmd == "start":
                self.start_sending()
            elif cmd and cmd == "stop":
                self.stop_sending()
        except:
            self.log.exception('Failed to parse payload')
