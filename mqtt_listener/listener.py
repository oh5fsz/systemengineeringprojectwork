import json
import os
import logging
import paho.mqtt.client as mqtt
from threading import Event
from influxdb import InfluxDBClient


logging.basicConfig(
    level=logging.getLevelName(os.environ.get('LOG_LEVEL', 'INFO')),
    format='[%(asctime)s] [%(levelname)8s] [%(name)17s]: %(message)s',
)


class MQTTListener(object):
    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.stop = Event()
        self.influx_client = InfluxDBClient(
            os.getenv('INFLUXDB_SERVER', 'sepw-influxdb'),
            int(os.getenv('INFLUXDB_PORT', 8086)),
            os.getenv('INFLUXDB_USER', ''),
            os.getenv('INFLUXDB_PASSWORD', ''),
            os.getenv('INFLUXDB_DB', 'sepw')
        )
        self.influx_client.create_database(os.getenv('INFLUXDB_DB', 'sepw'))
        self.client = mqtt.Client()
        self.client.connect(
            os.getenv('MQTT_SERVER', 'mqtt.naukkarinen.xyz'),
            int(os.getenv('MQTT_PORT', 1883)),
            60
        )
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

    def start(self):
        while not self.stop.is_set():
            self.client.loop()

    def on_connect(self, client, userdata, flags, rc):
        if (rc == 0):
            self.log.info("MQTT connected")
            client.subscribe('$queue/measurements/+')

    def on_message(self, client, userdata, msg):
        try:
            data = msg.payload[:-1].strip().decode('utf-8')
            data = json.loads(data)
        except:
            self.log.exception("Failed to parse JSON")
            return
        for measurement in data:
            data_to_write = []
            timestamp = measurement.get('timestamp', None)
            if not timestamp:
                self.log.error('Measurement had no timestamp')
                return
            self.log.info('Received data')
            if measurement.get('ac', None):
                data = measurement.get('ac', {})
                meas = {
                    'measurement': 'ac',
                    'time': timestamp,
                    'fields': {
                        'I_A': float(data.get('I_A', 'nan')),
                        'I_B': float(data.get('I_B', 'nan')),
                        'I_C': float(data.get('I_C', 'nan'))
                    }
                }
                data_to_write.append(meas)
                self.log.debug('Write AC')
            if measurement.get('modulator', None):
                self.log.debug('Write modulator')
                data = measurement.get('modulator', {})
                meas = {
                    'measurement': 'modulator',
                    'time': timestamp,
                    'fields': {
                        'Va': float(data.get('Va', 'nan')),
                        'Vb': float(data.get('Vb', 'nan'))
                    }
                }
                data_to_write.append(meas)
            if measurement.get('encoder', None):
                self.log.debug('Write encoder')
                data = measurement.get('encoder', {})
                meas = {
                    'measurement': 'encoder',
                    'time': timestamp,
                    'fields': {
                        'theta': float(data.get('theta', 'nan')),
                        'theta_el': float(data.get('theta_el', 'nan')),
                        'omega': float(data.get('omega', 'nan'))
                    }
                }
                data_to_write.append(meas)
            if measurement.get('control', None):
                self.log.debug('Write control')
                data = measurement.get('control', {})
                meas = {
                    'measurement': 'control',
                    'time': timestamp,
                    'fields': {
                        'mode': int(data.get('mode')),
                        'ref': float(data.get('ref', 'nan'))
                    }
                }
                data_to_write.append(meas)
            if "simulator" in msg.topic:
                tags = {
                    'motor': 'simulated'
                }
            else:
                tags = {
                    'motor': '1'
                }
            if not self.influx_client.write_points(data_to_write, tags=tags):
                self.log.error("Failed to write points")
            self.log.debug('Writing to DB: %s' % data_to_write)


if __name__ == "__main__":
    m = MQTTListener()
    m.start()
